# WordPress Theme Seed
## Install

```
yarn install
```

## Start
```
yarn start
```

## Production
```
yarn prod
```

## Authors

* **Luis Gago Casas** - *Initial work* - [gitlab.com/luisgagocasas](gitlab.com/luisgagocasas)

## License

This project is licensed under the MIT License.